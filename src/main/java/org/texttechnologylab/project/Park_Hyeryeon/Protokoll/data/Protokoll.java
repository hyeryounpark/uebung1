package org.texttechnologylab.project.Park_Hyeryeon.Protokoll.data;

import java.util.Set;
import java.sql.Date;

/**
 * Interface for representing a tweet.
 * @author Hyeryeon Park
 * @version 0.1
 */
public interface Protokoll {

    /**
     * Get Tagesordnungspunkte of Protokoll
     * @return String
     */
    String Tagesordnungspunkte();

    /**
     * Get Legislaturperiode of Protokoll
     * @return int
     */
    int getLegislaturperiode();

    /**
     * Get FortlaufendeNummer of Protokoll
     * @return int
     */
    int getFortlaufendeNummer();

    /**
     * Get Stizungsleiter of Abgeordnete
     * @return Abgeordnete
     */
    String getSitzungsleiter();

    /**
     * Get Datum of Protokoll
     * @return Date
     */
    Date getDatum();

    /**
     * Get Titel of Protokoll
     * @return String
     */
    String getTitel();

    /**
     * Get Tagesordnungspunkt of Protokoll
     * @return String
     */
    String getTagesordnungspunkt();

    /**
     * Return all mentioned Tagesordnungspunkten
     * @return Set<String>
     */
    Set<String> getTagesordnungspunkten();

}
