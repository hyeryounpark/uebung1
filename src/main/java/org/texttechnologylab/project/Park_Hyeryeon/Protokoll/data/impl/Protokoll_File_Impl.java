package org.texttechnologylab.project.Park_Hyeryeon.Protokoll.data.impl;

import org.texttechnologylab.project.Park_Hyeryeon.Protokoll.data.Kommentar;
import org.texttechnologylab.project.Park_Hyeryeon.Protokoll.data.Protokoll;
import java.lang.String;

public abstract class Protokoll_File_Impl implements Protokoll {
    private long Rede;
    private Kommentar kommentar;
    private long text;
    public String redner;


    public Protokoll_File_Impl(long Rede, Kommentar kommentar, long text, String redner){

        /**
         * keyword is to eliminate the confusion
         * between class attributes and parameters
         * with the same name
         */
        this.Rede = Rede;
        this.kommentar = kommentar;
        this.text = text;
        this.redner = redner;
    }

    @Override
    public String Tagesordnungspunkte() {
        return null;
    }
}
