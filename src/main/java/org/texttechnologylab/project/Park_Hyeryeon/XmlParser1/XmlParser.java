package org.texttechnologylab.project.Park_Hyeryeon.XmlParser1;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class XmlParser {

    private static final String FILENAME = "1.xml";

    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document document = db.parse(new File("org/texttechnologylab/project/Park_Hyeryeon/XmlParser1/1.xml"));
            document.getDocumentElement().normalize();
            //print Root Element
            System.out.println("Root Element :" + document.getDocumentElement().getNodeName());

            // get <kopfdaten>
            NodeList kopfList = document.getElementsByTagName("kopfdaten");
            for (int temp = 0; temp < kopfList.getLength(); temp++) {
                Node kNode = kopfList.item(temp);

                if (kNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) kNode;

                    // get text
                    String plenarprotokollNummer = element.getElementsByTagName("plenarprotokollNummer").item(0).getTextContent();
                    String wahlperiode = element.getElementsByTagName("wahlperiode").item(0).getTextContent();
                    String sitzungsnr = element.getElementsByTagName("sitzungsnr").item(0).getTextContent();
                    String herausgeber = element.getElementsByTagName("herausgeber").item(0).getTextContent();
                    String berichtart = element.getElementsByTagName("berichtart").item(0).getTextContent();
                    String sitzungstitel = element.getElementsByTagName("sitzungstitel").item(0).getTextContent();
                    String veranstaltungsdaten = element.getElementsByTagName("veranstaltungsdaten").item(0).getTextContent();
                    String ort = element.getElementsByTagName("ort").item(0).getTextContent();
                    String datum = element.getAttribute("datum");

                    System.out.println("\n Element Name :" + kNode.getNodeName());
                    System.out.println("\n Plenarprotokoll Nummer :" + plenarprotokollNummer);
                    System.out.println("\n Wahlperiode :" + wahlperiode);
                    System.out.println("\n Sitzungsnummer :" + sitzungsnr);
                    System.out.println("\n Herausgeber :" + herausgeber);
                    System.out.println("\n Bereichtart :" + berichtart);
                    System.out.println("\n Sitzungstitel :" + sitzungstitel);
                    System.out.println("\n Veranstaltungsdaten :" + veranstaltungsdaten);
                    System.out.println("\n Ort :" + ort);
                    System.out.println("\n Datum :" + datum);

                }
            }
            // get inhaltsverzeichnis
            NodeList inhaltNodeList = document.getElementsByTagName("inhaltsverzeichnis");
            for (int temp = 0; temp < inhaltNodeList.getLength(); temp++) {
                Node iNode = inhaltNodeList.item(temp);

                if (iNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) iNode;

                    // get text
                    String inhalt = inhaltNodeList.item(0).getTextContent();

                    String ivzTitel = element.getElementsByTagName("ivz-titel").item(0).getTextContent();

                    // get redner
                    NodeList rednerList = document.getElementsByTagName("redner");


                    for (int temp1 = 0; temp1 < rednerList.getLength(); temp1++) {

                        Node rnode = rednerList.item(temp1);

                        if (rnode.getNodeType() == Node.ELEMENT_NODE) {

                            Element element1 = (Element) rnode;

                            // get redner's attribute
                            String redner = element1.getAttribute("redner");

                            // get text
                            String vorname = element1.getElementsByTagName("Vorname").item(0).getTextContent();
                            String nachname = element1.getElementsByTagName("nachname").item(0).getTextContent();
                            String rolleLang = element1.getElementsByTagName("rolle_lang").item(0).getTextContent();
                            String rolleKurz = element1.getElementsByTagName("rolle_kurz").item(0).getTextContent();

                            // get <xref>
                            NodeList xrefList = document.getElementsByTagName("xref");

                            for (int temp2 = 0; temp2 < xrefList.getLength(); temp2++) {

                                Node xnode = xrefList.item(temp2);

                                if (xnode.getNodeType() == Node.ELEMENT_NODE) {

                                    Element element2 = (Element) xnode;

                                    // get xref's attribute
                                    String div = element2.getAttribute("div");
                                    String pnr = element2.getAttribute("pnr");
                                    String refType = element2.getAttribute("ref-type");
                                    String rid = element2.getAttribute("rid");

                                    // get <a>
                                    NodeList aList = document.getElementsByTagName("a");

                                    for (int temp3 = 0; temp < aList.getLength(); temp3++) {

                                        Node anode = aList.item(temp3);

                                        if (anode.getNodeType() == Node.ELEMENT_NODE) {

                                            Element element3 = (Element) anode;

                                            // get a's attribute
                                            String href = element3.getAttribute("href");
                                            String typ = element3.getAttribute("typ");

                                            // get text
                                            String seite = element3.getElementsByTagName("seite").item(0).getTextContent();
                                            String seitenbereich = element3.getElementsByTagName("seitenbereich").item(0).getTextContent();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();}
    }
}
